/*---------------- ADD LIB --------------------*/

const express = require("express");
const path = require("path");
const app = express();
const { port } = require("./config/env");
const mongoose = require("./config/mongoose");
const bodyParser = require("body-parser");
const routes = require('./route');

/* ------- ROUTE -------- */
app.use("/", routes);
/*---------------- END ADD LIB --------------------*/
mongoose.connect(); /*  connect mongo db */

app.get("/", (req, res) => res.send("Hello World!"));

/* view engine setup - view */
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

app.listen(port, () =>
  console.log(`App listening at http://localhost:${port}`)
);
